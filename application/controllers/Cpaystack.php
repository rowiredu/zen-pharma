<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cpaystack extends CI_Controller {
	public $menu;
	function __construct() {
      parent::__construct();
		$this->load->library('auth');
		$this->load->library('lpaystack');
		$this->load->library('session');
		$this->load->model('Paystack');
		$this->auth->check_admin_auth();
		$this->template->current_menu = 'paystack';

//		if ($this->session->userdata('user_type') == '2') {
//            $this->session->set_userdata(array('error_message'=>display('you_are_not_access_this_part')));
//            redirect('Admin_dashboard');
//        }
    }
	public function index()
	{
		$content = $this->lpaystack->setting_add_form();
		$this->template->full_admin_html_view($content);
	}
	// Update setting
	public function update_setting()
	{
		$this->load->model('Paystack');
	
		$data=array(
			'private_key' 		=> $this->input->post('private_key'),
			'public_key' 	=> $this->input->post('public_key'),
			'fees_option' => $_POST['optradio'],
		);

		$this->Paystack->update_setting($data);

		$this->session->set_userdata(array('message'=>display('successfully_updated')));
		redirect(base_url('Cpaystack'));
		exit;
	}

	public function paystack_transaction() {
		$setting_detail = $this->Paystack->retrieve_setting_editdata();
		$refsuffix = rand() * 10000;
        $timestamp = time();
		$prefix = 'ZEN-POS';
		$private_key = $setting_detail[0]['private_key'];
		$fees_option = $setting_detail[0]['fees_option'];
        $ref = $prefix.'-'.$timestamp.'-'.$refsuffix;
        $paid = $this->input->post('grand_total_price');
		$charge = ($paid * 0.0195);
		$charge = number_format ($charge , 2);
		$scharge = ($paid * 0.0100);
		$scharge = number_format ($scharge , 2);
		$email = $ref.'@gmail.com';
		$authorization = 'Bearer '.$private_key;
		
        if($fees_option == "1") {
            $paid = ($paid + $charge) * 100;
        }else if($fees_option == "2") {
            $paid = $paid * 100;
		}else {
			$paid = ($paid + $scharge) * 100;
		}

		$url = "https://api.paystack.co/transaction/initialize";
		$fields = [
		  'email' => $email,
		  'amount' => $paid,
		  'currency' => 'GHS',
		  'reference' => $ref,
		];
		$fields_string = http_build_query($fields);
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Authorization: $authorization",
		  "Cache-Control: no-cache",
		));
		
		//So that curl_exec returns the contents of the cURL; rather than echoing it
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true); 
		
		//execute post
		$result = curl_exec($ch);
		echo $result;
		//echo $email;
	}

	public function verify_paystack() {
		$setting_detail = $this->Paystack->retrieve_setting_editdata();
		$private_key = $setting_detail[0]['private_key'];
		$authorization = 'Bearer '.$private_key;
		$curl = curl_init();
		$reference = $_POST['ref'];
		$url = "https://api.paystack.co/transaction/verify/$reference";
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
			"Authorization: $authorization",
			"Cache-Control: no-cache",
		  ),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		  //echo "response verfify";
		}
	}

	public function insert_mobile_money() {
		$this->load->model('Paystack');

		$amount = ($_POST['amount']/100);
		$data = array(
			'status' => $_POST['status'],
			'message' => $_POST['message'],
			'amount' => $amount,
			'reference' => $_POST['reference'],
			'transaction_date' => $_POST['transaction_date'],
			'mobile_number' => $_POST['mobile_number']
		);

		$this->Paystack->insert_mobile_money($data);
    }
}