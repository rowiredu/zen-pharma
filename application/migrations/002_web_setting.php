<?php
class Migration_Create_web_setting extends CI_Migration {
	
	public function up()
	{
		$fields = array(
			'public_key' => array(
				'type'           => 'VARCHAR',
				'constraint'     => '255',
                'null' => TRUE
			),
			'private_key' => array(
				'type'       => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
			),
			'fees_option' => array(
				'type'       => 'VARCHAR',
                'constraint' => '11',
                'default' => '1',
                'null' => FALSE
            ),
        );

        $this->dbforge->add_column('web_setting', $fields);
	}

	public function down()
	{
        $this->dbforge->drop_column('web_setting', 'public_key');
        $this->dbforge->drop_column('web_setting', 'private_key');
        $this->dbforge->drop_column('web_setting', 'fees_option');
	}
}