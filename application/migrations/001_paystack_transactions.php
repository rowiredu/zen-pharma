<?php
class Migration_Create_paystack_transactions extends CI_Migration {
	
	public function up()
	{
		$this->dbforge->add_field (array(
			'id' => array(
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			),
			'status' => array(
				'type'       => 'VARCHAR',
				'constraint' => '60'
			),
			'message' => array(
				'type'       => 'VARCHAR',
				'constraint' => '254'
			),
			'amount' => array(
                'type' => 'VARCHAR',
                'constraint' => '60'
			),
			'reference' => array(
                'type' => 'VARCHAR',
                'constraint' => '150'
            ),
            'transaction_date' => array(
                'type' => 'VARCHAR',
                'constraint' => '60'
            ),
            'mobile_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '60'
            ),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('paystack_transactions');
	}

	public function down()
	{
		$this->dbforge->drop_table('paystack_transactions');
	}
}