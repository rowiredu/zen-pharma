<!-- Add new customer start -->
<div class="content-wrapper">
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-note2"></i>
        </div>
        <div class="header-title">
            <h1><?php echo display('update_setting') ?></h1>
            <small><?php echo display('update_your_web_setting') ?></small>
            <ol class="breadcrumb">
                <li><a href="#"><i class="pe-7s-home"></i> <?php echo display('home') ?></a></li>
                <li><a href="#"><?php echo display('web_settings') ?></a></li>
                <li class="active"><?php echo display('update_setting') ?></li>
            </ol>
        </div>
    </section>

    <section class="content">
        <!-- Alert Message -->
        <?php
            $message = $this->session->userdata('message');
            if (isset($message)) {
        ?>
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('message');
            }
            $error_message = $this->session->userdata('error_message');
            if (isset($error_message)) {
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $error_message ?>                    
        </div>
        <?php 
            $this->session->unset_userdata('error_message');
            }
        ?>

        <?php
        if($this->permission1->method('soft_setting','read')->access() || $this->permission1->method('soft_setting','update')->access()){ ?>
            <!-- New customer -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-bd lobidrag">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4><?php echo display('update_setting') ?> </h4>
                        </div>
                    </div>
                  <?php echo form_open_multipart('Cpaystack/update_setting', array('class' => 'form-vertical','id' => 'insert_settings'))?>
                    <div class="panel-body">

                        <div class="form-group row">
                            <label for="fees" class="col-sm-3 col-form-label"><?php echo "Who should pay the transactions fees?" ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6" style="display: flex; flex-direction: column;">
                                <span style="margin-bottom: 8px; cursor: pointer;"><input style="cursor: pointer;" type="radio" name="optradio" value="1" id="radio_1"> Make customers pay the transaction fees</span>
                                <span style="margin-bottom: 8px; cursor: pointer;"><input style="cursor: pointer;" type="radio" name="optradio" value="2" id="radio_2"> Charge me for the transaction fees</span>
                                <span style="margin-bottom: 8px; cursor: pointer;"><input style="cursor: pointer;" type="radio" name="optradio" value="3" id="radio_3"> Share transaction fees with customer</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="private_key" class="col-sm-3 col-form-label"><?php echo "Private key" ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="private_key" id="private_key" type="text" placeholder="<?php echo "Private key" ?> " value="{private_key}" tabindex="11">
                            </div>
                        </div>    
                        
                        <div class="form-group row">
                            <label for="public_key" class="col-sm-3 col-form-label"><?php echo "Public key" ?> <i class="text-danger">*</i></label>
                            <div class="col-sm-6">
                                <input class="form-control" name ="public_key" id="public_key" type="text" placeholder="<?php echo "Public key" ?>" value="{public_key}" tabindex="12">
                            </div>
                        </div>

                        <?php
                        if($this->permission1->method('language','update')->access()){ ?>
                         <div class="form-group row">
                            <label for="example-text-input" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-6">
                                <input type="submit" id="add-customer" class="btn btn-success btn-large" name="add-customer" value="<?php echo display('save_changes') ?>" tabindex="13"/>
                            </div>
                         </div>
                        <?php } ?>

                    </div>
                    <?php echo form_close()?>
                </div>
            </div>
        </div>
        <?php }
        else{
        ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-bd lobidrag">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h4><?php echo display('You do not have permission to access. Please contact with administrator.');?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }?>


    </section>
</div>
<!-- Add new customer end -->

<script>
    $(document).ready(function(){
        //alert('{fees_option}');
        if('{fees_option}' == 1) {
            $("#radio_1").prop("checked", true);
        }else if('{fees_option}' == 2) {
            $("#radio_2").prop("checked", true); 
        }else {
            $("#radio_3").prop("checked", true);
        }
    });
</script>